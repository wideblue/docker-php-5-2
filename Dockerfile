FROM ubuntu:16.04

LABEL maintainer="Damjan Dvorsek"

RUN apt-get update && apt-get -y install software-properties-common

# Install Apache2 / PHP 5.2 & Co.

RUN add-apt-repository ppa:sergey-dryabzhinsky/php52 && apt-get update \
&& apt-get -y install apache2 php5 libapache2-mod-php5 php5-dev php5-curl php5-xml php5-simplexml php-pear curl libaio-dev libaio1 alien python3-dev build-essential unzip

# Install the Oracle Instant Client    
ADD oracle/oracle-instantclient11.2-basic-11.2.0.4.0-1.x86_64.rpm /tmp
ADD oracle/oracle-instantclient11.2-devel-11.2.0.4.0-1.x86_64.rpm /tmp

RUN alien -i /tmp/oracle-instantclient11.2-basic-11.2.0.4.0-1.x86_64.rpm && alien -i /tmp/oracle-instantclient11.2-devel-11.2.0.4.0-1.x86_64.rpm
#RUN dpkg -i /tmp/oracle-instantclient12.1-sqlplus_12.1.0.2.0-2_amd64.deb
#RUN rm -rf /tmp/oracle-instantclient12.1-*.deb

# Set up the Oracle environment variables
ENV LD_LIBRARY_PATH /usr/lib/oracle/11.2/client64/lib/
ENV ORACLE_HOME /usr/lib/oracle/11.2/client64/

# Install the OCI8 PHP extension
RUN echo 'instantclient,/usr/lib/oracle/11.2/client64/lib' | pecl install -f oci8-2.0.12
# RUN echo "extension=oci8.so" > /etc/php5/apache2/conf.d/30-oci8.ini
RUN echo extension=oci8.so >> /etc/php52/apache2/php.ini && echo extension=oci8.so >> /etc/php52/cli/php.ini && \
echo extension=simplexml.so >> /etc/php52/apache2/php.ini && echo extension=simplexml.so >> /etc/php52/cli/php.ini && \
echo extension=xml.so >> /etc/php52/apache2/php.ini && echo extension=xml.so >> /etc/php52/cli/php.ini

# Enable Apache2 modules
# In the latest Apache 2.0, for Ubuntu 16.04 that Apache is pre-configured with threads enabled, but the PHP 5 is not. Workaround a2dismod mpm_event && a2enmod mpm_prefork
# RUN a2enmod rewrite
RUN a2dismod mpm_event && a2enmod php52 && a2enmod mpm_prefork

# Set up the Apache2 environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

EXPOSE 80
COPY sample/index.php /var/www/html/
# Run Apache2 service
CMD service apache2 start
